package com.example.beatstore.repository;

import com.example.beatstore.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User user;


    @BeforeEach
    void setUp() {
        user = User.builder()
                .id(1l)
                .email("justemail@gmail.com")
                .build();
        userRepository.save(user);
    }

    @AfterEach
    void tearDown() {
        user = null;
        userRepository.deleteAll();
    }

    @Test
    void testFindByEmail_Found() {
        Optional<User> userOptional = userRepository.findByEmail("justemail@gmail.com");
        User user = userOptional.orElseGet(User::new);
        assertThat(user.getId()).isEqualTo(this.user.getId());
    }
}
