package com.example.beatstore.error;

public class BeatNotFoundException extends Exception {
    public BeatNotFoundException() {
        super();
    }

    public BeatNotFoundException(String message) {
        super(message);
    }

    public BeatNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeatNotFoundException(Throwable cause) {
        super(cause);
    }

    protected BeatNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
