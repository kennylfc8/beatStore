package com.example.beatstore.repository;

import com.example.beatstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

//why the is no @Component annotation ?
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

}
