package com.example.beatstore.service;

import com.example.beatstore.model.Beat;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BeatService {
    List<Beat> getBeats();

    void saveBeat(Beat beat);
}
