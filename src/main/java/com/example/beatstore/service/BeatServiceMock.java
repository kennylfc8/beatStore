package com.example.beatstore.service;

import com.example.beatstore.model.Beat;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeatServiceMock implements BeatService{
    @Override
    public List<Beat> getBeats() {
        return null;
    }

    @Override
    public void saveBeat(Beat beat) {

    }
}
