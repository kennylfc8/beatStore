package com.example.beatstore.service;


import com.example.beatstore.model.Beat;
import com.example.beatstore.model.Cart;
import com.example.beatstore.repository.CartRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Getter
@Setter
public class CartService {

    private CartRepository cartRepository;

    @Autowired
    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void addBeat(Beat beat) {

    }

    public void removeBeat(Beat beat) {

    }

    public void clearCart() {

    }

    public Optional<Cart> getCart(Long id) {
        return cartRepository.findById(id);
    }

    public Optional<List<Cart>> getCarts() {
        return Optional.of(cartRepository.findAll());
    }

}
