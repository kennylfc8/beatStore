package com.example.beatstore.service;

import com.example.beatstore.error.BeatNotFoundException;
import com.example.beatstore.model.Beat;
import com.example.beatstore.repository.BeatRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
@AllArgsConstructor
@Primary
public class BeatServiceImpl implements BeatService {

    private final BeatRepository beatRepository;

//    @Autowired
//    EntityManager em;


    public List<Beat> getBeats() {
        return beatRepository.findAll();
    }

    public Beat getBeat(Long id) throws BeatNotFoundException {
        return beatRepository.findById(id)
                .orElseThrow(() -> new BeatNotFoundException("Beat not found with id " + id));
    }

    public void saveBeat(Beat beat) {
        beatRepository.save(beat);
//        em.merge(beat);
    }
}
