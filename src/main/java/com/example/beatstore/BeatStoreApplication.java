package com.example.beatstore;

//import com.example.beatstore.soap.BeatGetterImpl;
import org.hibernate.cfg.annotations.reflection.PersistentAttributeFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Endpoint;

@SpringBootApplication
public class BeatStoreApplication implements CommandLineRunner {
//
//    @Autowired
//    private BeatGetterImpl beatGetter;

    public static void main(String[] args) {
        SpringApplication.run(BeatStoreApplication.class, args);
    }

    @Override
    public void run(String... args) {
//        Endpoint.publish("http://localhost:8090/beatGetter", beatGetter);
//        System.out.println("SERVICE IS PUBLISHED");
    }
}