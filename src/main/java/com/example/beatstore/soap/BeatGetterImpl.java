//package com.example.beatstore.soap;
//
//import com.example.beatstore.model.Beat;
//import com.example.beatstore.model.Cart;
//import com.example.beatstore.service.BeatService;
//import com.example.beatstore.service.CartService;
//import lombok.AllArgsConstructor;
//import lombok.NoArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.jws.WebService;
//import java.util.List;
//
//@Service
//@WebService(endpointInterface = "com.example.beatstore.soap.BeatGetter")
//@AllArgsConstructor
//@NoArgsConstructor
//public class BeatGetterImpl implements BeatGetter {
//
//    @Autowired
//    CartService cartService;
//
//    @Override
//    public List<Cart> getBeats() {
//        return cartService.getCarts()
//                .orElseThrow();
//    }
//
//    @Override
//    public Beat getBeat(int id) {
//        return new Beat();
//    }
//
//}
