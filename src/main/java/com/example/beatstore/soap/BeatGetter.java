package com.example.beatstore.soap;

import com.example.beatstore.model.Beat;
import com.example.beatstore.model.Cart;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import javax.jws.WebMethod;
import java.util.List;

@WebService
@Service
public interface BeatGetter {
    @WebMethod
    List<Cart> getBeats();
    @WebMethod
    Beat getBeat(int id);

}
