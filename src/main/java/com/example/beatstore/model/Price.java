package com.example.beatstore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "price", schema = "beat_store")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column
    private Long id;

    private float mp3;

    private float wav;

    private float wavTrackout;

    private float unlimited;

    private float exclusive;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "price")
    private List<Beat> beats;
}
