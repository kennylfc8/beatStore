package com.example.beatstore.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name = "beat", schema = "beat_store")
@Getter
@Setter
public class Beat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Min(10)
    @Max(500)
    private int bpm;


    private String key;

    private String name;

    private LocalTime time;

    private String wavPath;

    private String mp3Path;

    private String picturePath;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "Beat_Tag",
            schema = "beat_store",
            joinColumns = {@JoinColumn(name = "beat_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private List<Tag> tags;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "price_id", referencedColumnName = "id")
    private Price price;
}
