package com.example.beatstore.model;

public enum Role {
    GUEST,
    ARTIST,
    LOGGED_USER
}
