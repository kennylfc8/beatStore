package com.example.beatstore.config;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class StartupSqlRunner implements ApplicationListener<ApplicationReadyEvent> {

    private final DataSource dataSource;
    private final ResourceLoader resourceLoader;
    private static final String DATA_SQL = "classpath:data.sql";

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = loadSqlScript();
        jdbcTemplate.execute(sql);
    }

    private String loadSqlScript() {
        Resource resource = resourceLoader.getResource(DATA_SQL);
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)
        )) {
            return reader.lines()
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load SQL script: " + "classpath:data.sql", e);
        }

    }
}
