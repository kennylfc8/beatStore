package com.example.beatstore.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/demo-contr")
public class DemoController {
    @GetMapping
    public ResponseEntity<String> sayHell(){
        return ResponseEntity.ok("Hello from secured endpoint");
    }
}
