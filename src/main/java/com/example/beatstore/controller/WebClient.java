package com.example.beatstore.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
//TODO: delete this class
@Component
public class WebClient {

    ResponseEntity<String> response;

//    public ResponseEntity<String> getResponse() {
//        RestTemplate restTemplate = new RestTemplate();
//
//        String url = "https://liverpoolfc.ru/";
//
//        response = restTemplate.getForEntity(url, String.class);
//        return response;
//    }

    public ResponseEntity<String> getResponse() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://www.youtube.com/";

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537");

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        return response;
    }

}
