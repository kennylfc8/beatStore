package com.example.beatstore.controller;

import com.example.beatstore.model.Beat;
import com.example.beatstore.service.BeatService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

//to accept all connections from all urls
// TODO: 21.02.2022 fix CrossOrigin to only allowed applications  
@CrossOrigin
@RestController
@Getter
@Setter
@Slf4j
@Validated
public class BeatController {

    private BeatService beatService;


    public BeatController(BeatService beatService) {
        this.beatService = beatService;
    }
   

    @GetMapping(value = "/beats")
    List<Beat> getBeats() {
        log.info("getting beats from beatController");
        return beatService.getBeats();
    }

    @PostMapping(value = "/beat")
    void postBeat(@Valid @RequestBody Beat newBeat) {
        beatService.saveBeat(newBeat);
    }
}
