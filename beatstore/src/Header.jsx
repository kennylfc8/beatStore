import React from 'react'
import LogInButton from "./LoginButton";
import RegisterButton from "./RegisterButton";

function Header() {
    return (
        <>
            <div className={'beat-store'}>
                BEAT STORE
            </div>
            <div className={'auth-buttons-container'}>
                <div className={'register-button'}>
                    <RegisterButton />
                </div>
                <div className={'login-button'}>
                    <LogInButton />
                </div>
            </div>
        </>
    );
}

export default Header;