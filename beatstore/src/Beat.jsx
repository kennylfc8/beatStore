import React, {useState} from 'react';

const Beat = ({beat: {id, bpm, key, name, wavPath, mp3Path, picturePath, tags, price}}) => {
    const [isSelected, setIsSelected] = useState(false);

    const handleClick = () => {
        setIsSelected(!isSelected);
    };

    return (
        <div
            className={`beat${isSelected ? ' beat-selected' : ''}`}
            onClick={handleClick}
        >
            <div>
                <h1>
                    {name}, bpm {bpm}, tags {tags.map(tag => tag.name)}, mp3 price {price.mp3}, wav
                    price {price.wav}, wavTrackout price {price.wavTrackout}, unlimited price {price.unlimited},
                    exclusive price {price.exclusive}
                </h1>
            </div>
        </div>
    );
};

export default Beat;
