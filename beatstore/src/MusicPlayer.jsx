import {useEffect, useState} from 'react';

function MusicPlayer(props) {
    const [audio] = useState(new Audio(props.src));
    const [isPlaying, setIsPlaying] = useState(false);

    useEffect(() => {
        const playAudio = () => {
            audio.play();
            setIsPlaying(false);
        };
        const loadAudio = () => {
            audio.removeEventListener('canplaythrough', loadAudio);
            playAudio();
        };

        audio.addEventListener('canplaythrough', loadAudio);
        return () => {
            audio.pause();
            audio.currentTime = 0;
            audio.removeEventListener('canplaythrough', loadAudio);
        };
    }, [audio, props.src]);

    const handlePlayClick = () => {
        if (!isPlaying) {
            audio.play();
            setIsPlaying(true);
        }
    };

    const handlePauseClick = () => {
        if (isPlaying) {
            audio.pause();
            setIsPlaying(false);
        }
    };

    return (
        <div>
            <div>MUSIC PLAYER</div>
            <button onClick={handlePlayClick} disabled={isPlaying}>
                Play
            </button>
            <button onClick={handlePauseClick} disabled={!isPlaying}>
                Pause
            </button>
        </div>
    );
}

export default MusicPlayer;