import React from 'react';

import {API_URL} from "./constants";

const RegisterButton = ({firstname, lastname, email, password}) => {
    const handleClick = () => {
        fetch(`${API_URL}/api/v1/auth/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({firstname, lastname, email, password})
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    return (
        <button
            className={'register-button'}
            onClick={handleClick}
        >
            Register
        </button>
    )
}

export default RegisterButton;