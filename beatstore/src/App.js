import React, {useEffect, useState} from "react";
import Beat from './Beat';
import MusicPlayer from './MusicPlayer'
import { API_URL } from './constants';

import './App.css'
import Header from "./Header";

//useEffect??? await ??? async? ? ??
function App() {
    const [beats, setBeats] = useState([]);

    useEffect(() => {
        const getBeats = async () => {
            const response = await fetch(`${API_URL}/beats`);
            const data = await response.json();
            setBeats(data);
        };
        getBeats();
    }, []);

    return (
        <div className='app'>
            <div className='header'>
                <Header />
            </div>
            <div className='beat-list'>
                {beats.map((beat) => (
                    <Beat key={beat.id} beat={beat}/> // Make sure to use a unique 'key' prop for better performance
                ))}
            </div>
            <div className='music-player'>
                <MusicPlayer
                    src='https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3'/>
            </div>
        </div>
    );
}

export default App;
