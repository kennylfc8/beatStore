import React, {useState} from 'react';

import { API_URL } from './constants';

const LogInButton = ({username, password}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const handleClick = () => {
        setIsLoading(true);

        fetch( `${API_URL}/api/v1/auth/authenticate`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({username, password}),
        })
            .then((response) => response.json())
            .then((data) => {
                setIsLoading(false);
                setIsLoggedIn(true);
                // Do something with the response data (token, user info, etc.)
                console.log(data);
            })
            .catch((error) => {
                setIsLoading(false);
                console.log(error);
                // Handle the error
            });
    };

    return (
        <button
            className={`login-button${isLoading ? ' button-loading' : ''}`}
            onClick={handleClick}
            disabled={isLoading}
        >
            {isLoading ? "Logging in..." : isLoggedIn ? "Logged In" : "Log In"}
        </button>
    );
};

export default LogInButton;
