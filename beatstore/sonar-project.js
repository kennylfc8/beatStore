const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner(
    {
        serverUrl: 'http://localhost:9000',
        options: {
            'sonar.sources': 'src',
            // 'sonar.tests': '__tests__',
            'sonar.inclusions': '**',
            // 'sonar.test.inclusions': '__tests__/**/*.test.js,__tests__/**/*.test.jsx',
            'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
            // 'sonar.testExecutionReportPaths': 'reports/test-report.xml',
            'sonar.login': 'admin',
            'sonar.password': 'ufK2gX#@hX43ijx'
        },
    },
    () => {},
);
